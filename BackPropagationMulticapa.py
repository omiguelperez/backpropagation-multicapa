# -*- coding: utf-8 -*-

import math
import random
import pickle
from pylab import ylim, show, plot, ion

MIN = -1.0
MAX = 1.0


class Console(object):
    def pause(self):
        input('PRESIONE ENTER PARA CONTINUAR...')

    def pause_with_message(self, message):
        print('\n{}'.format(message))
        input('PRESIONE ENTER PARA CONTINUAR...')


class BackpropagationMulticapa:
    def __init__(self, cantidad_entradas, cantidad_salidas, neuronas_capas, funciones_activacion_capas, funcion_activacion_salida):
        self.console = Console()
        self.m = cantidad_entradas
        self.n = cantidad_salidas
        self.neuronas_capas = neuronas_capas # [100, 80, ...]
        self.funciones_activacion_capas = funciones_activacion_capas # ['logsig', 'logsig', ...]
        self.cantidad_capas = len(neuronas_capas)
        self.funciones_activacion_salida = funcion_activacion_salida # 'purelin'
        self.cantidad_patrones = None # Cantidad de patrones
        self.errores = []
        self.inicializar_pesos()
        self.inicializar_umbrales()
        self.inicializar_parametros_de_entrenamiento()

    def inicializar_pesos(self):
        self.wc = []
        for c in range(self.cantidad_capas + 1):
            if c == 0:
                self.wc.insert(len(self.wc), [[random.uniform(MIN, MAX) for i in range(self.neuronas_capas[c])] for j in range(self.m)])
            elif c == self.cantidad_capas:
                self.wc.append([[random.uniform(MIN, MAX) for i in range(self.n)] for j in range(self.neuronas_capas[c - 1])])
            else:
                self.wc.append([[random.uniform(MIN, MAX) for i in range(self.neuronas_capas[c])] for j in range(self.neuronas_capas[c-1])])
        # self.imprimir_pesos()
        # self.mostrar_info_pesos()

    def mostrar_info_pesos(self):
        print('\n\n{t} INFORMACION PESOS {t}'.format(t='*'*20))
        print('Matrices de pesos: {}'.format(len(self.wc)))
        for i, w in enumerate(self.wc):
            print('Martriz: {}, filas: {}, columnas: {}'.format(i+1, len(w), len(w[0])))
        input('PRESIONE ENTER PARA CONTINUAR...')

    def imprimir_pesos(self):
        for c in range(len(self.wc)):
            print("%s MATRIZ DE PESOS %s %s" % ('*'*30, (c + 1), '*'*30))
            pesos = self.wc[c]
            for i in range(len(pesos[0])):
                row = []
                for j in range(len(pesos)):
                    row.append(pesos[j][i])
                    print(row)

    def mostrar_info_umbrales(self):
        print('\n\n{t} INFORMACION UMBRALES {t}'.format(t='*'*20))
        print('Vectores de umbrales: {}'.format(len(self.uc)))
        for i, u in enumerate(self.uc):
            print('Capa: {}, umbrales: {}'.format(i+1, len(u)))
        input('PRESIONE ENTER PARA CONTINUAR...')

    def inicializar_umbrales(self):
        self.uc = []
        for capa in range(self.cantidad_capas):
            self.uc.insert(len(self.uc), [random.uniform(-1.0, 1.0) for i in range(self.neuronas_capas[capa])])
        self.uc.insert(len(self.uc), [random.uniform(-1.0, 1.0) for i in range(self.n)])
        # self.imprimir_umbrales()
        # self.mostrar_info_umbrales()

    def imprimir_umbrales(self):
        for c in range(len(self.uc)):
            print("%s VECTOR DE UMBRALES %s %s" % ('*'*30, (c + 1), '*'*30))
            print(self.uc[c])

    def definir_parametros_auxiliares(self):
        self.h = [[0 for j in range(self.neuronas_capas[i])] for i in range(self.cantidad_capas)]
        self.y = [[0.0 for p in range(self.cantidad_patrones)] for i in range(self.n)] # y[[salida 1], [salida 2], ...]
        self.el = [[0.0 for i in range(self.n)] for p in range(self.cantidad_patrones)]
        self.enl = [[0.0 for e in range(u)] for u in self.neuronas_capas]
        self.ep = [0.0 for p in range(self.cantidad_patrones)] # ep[errores...]
        self.rms = 0.0
        # self.mostrar_parametros_auxiliares()

    def mostrar_parametros_auxiliares(self):
        print('\n\n{t} PARAMETROS AUXILIARES {t}'.format(t='*'*20))
        print('\nSalidas entre capas (h)')
        for i, enl in enumerate(self.h):
            print('{} H en la capa {}'.format(len(enl), i+1))
        print('\nSalidas de la red')
        print('Salidas: {}, por cada patron ({} patrones)'.format(len(self.y), len(self.y[0])))
        print('\nErrores lineales producidos en la salida')
        print('Para los patrones hay {} errores lineales'.format(len(self.el[0])))
        print('\nErrores no lineales')
        for i, enl in enumerate(self.enl):
            print('Vector: {}, Errores NL: {}'.format(i+1, len(enl)))
        print('\nErrores producidos en los patrones')
        print('Patrones: {}'.format(len(self.ep)))
        print('\nErrores RMS')
        print('Inicia en: {}'.format(self.rms))
        input('PRESIONA UNA TECLA PARA CONTINUAR...')

    def inicializar_parametros_de_entrenamiento(self):
        self.trainParam = {}
        self.trainParam['epochs'] = 1000
        self.trainParam['goal'] = 0.00
        self.trainParam['lr'] = 0.01

    def definir_cantidad_patrones(self, cantidad_patrones = None):
        self.cantidad_patrones = len(self.x) if cantidad_patrones == None else cantidad_patrones

    def calcular_salida_de_la_red(self, patron):
        for c in range(self.cantidad_capas):
            entradas = self.m if c == 0 else self.neuronas_capas[c - 1]
            for i in range(self.neuronas_capas[c]):
                output = 0.0
                for j in range(entradas):
                    output += self.x[patron][j] * self.wc[c][j][i]
                output -= self.uc[c][i]
                self.h[c][i] = self.aplicar_funcion_de_activacion(self.funciones_activacion_capas[c], output)
        for i in range(self.n):
            output = 0.0
            for j in range(self.neuronas_capas[self.cantidad_capas - 1]):
                output += self.x[patron][j] * self.wc[len(self.wc) - 1][j][i]
            output -= self.x[patron][j] * self.uc[len(self.uc) - 1][i]
            self.y[i][patron] = self.aplicar_funcion_de_activacion(self.funciones_activacion_salida, output)
        # self.mostrar_salidas_calculadas_en_la_red(patron)

    def mostrar_salidas_calculadas_en_la_red(self, patron):
        for c in range(self.cantidad_capas):
            print('%s SALIDAS DE LA CAPA (H) %s DEL PATRON %s %s' % ('='*30, c + 1, patron + 1, '='*30))
            row = []
            for i in range(len(self.h[c])):
                row.append(self.h[c][i])
        for i in range(len(self.y)):
            row.append(self.y[i][patron])
        print(row)

    def aplicar_funcion_de_activacion(self, capa, valor):
        if capa == 'tanh':
            return math.tanh(valor)
        elif capa == 'sigmoid':
            try:
                return 1 / (1 + math.exp(-valor))
            except:
                return 0

    def calcular_error_lineal(self, patron):
        for i in range(self.n):
            self.el[patron][i] = self.yd[i][patron] - self.y[i][patron]
        # self.mostrar_errores_lineales_del_patron(patron)

    def mostrar_errores_lineales_del_patron(self, patron):
        print('%s ERRORES LINEALES DEL PATRON %s %s' % ('%'*30, patron + 1, '%'*30))
        print(self.el[patron])

    def calcular_errores_no_lineales(self, patron):
        for pos in reversed(range(self.cantidad_capas)):
            if self.cantidad_capas == pos + 1:
                # print('Ultima capa {},{}'.format(self.neuronas_capas[pos], self.n))
                for l in range(self.neuronas_capas[pos]):
                    sum = 0
                    for k in range(self.n):
                        sum += self.el[patron][k] * self.wc[pos][l][k]
                    self.enl[pos][l] = sum
            else:
                # print('Capa intermedia {}, {}'.format(self.neuronas_capas[pos + 1], self.neuronas_capas[pos]))
                for i in range(self.neuronas_capas[pos + 1]):
                    sum = 0
                    for l in range(self.neuronas_capas[pos]):
                        sum += self.enl[pos + 1][i] * self.wc[pos+1][l][i]                        
                    self.enl[pos][i] = sum

    def calcular_error_del_patron(self, patron):
        self.ep[patron] = sum([abs(e) for e in self.el[patron]]) / self.n           
        # self.mostrar_errores_por_patron(patron)

    def mostrar_errores_por_patron(self, patron):
        print('%s ERRORES PRODUCIDOS EN EL PATRON %s %s' % ('*'*10, patron + 1, '*'*10))
        print(self.ep[patron])

    def derivada_funcion_de_activacion(self, value):
        if self.funcion_de_activacion == 'tanh':
            return 1 / math.pow(math.cosh(value), 2) # 1 / (cosh(x))^2
        elif self.funcion_de_activacion == 'sigmoid':
            return math.exp(value) / math.pow(math.exp(value) + 1, 2) # e^t / (e^t + 1)^2

    def actualizar_pesos_y_umbrales(self, patron):
        for c in range(len(self.wc)):
            if c == 0:
                self.funcion_de_activacion = self.funciones_activacion_capas[c]
                for i in range(self.neuronas_capas[c]):
                    for j in range(self.m):
                        self.wc[c][j][i] += 2 * self.trainParam['lr'] * self.enl[c][i] * self.derivada_funcion_de_activacion(self.h[c][i]) * self.x[patron][j]
                    self.uc[c][i] += 2 * self.trainParam['lr'] * self.enl[c][i] * self.derivada_funcion_de_activacion(self.h[c][i]) * 1
            elif c == len(self.wc) - 1:
                self.funcion_de_activacion = self.funciones_activacion_salida
                
                for i in range(self.n):
                    for j in range(self.neuronas_capas[len(self.neuronas_capas) - 1]):
                        self.wc[c][j][i] += 2 * self.trainParam['lr'] * self.el[patron][i] * self.y[i][patron]
                    self.uc[c][i] += 2 * self.trainParam['lr'] * self.el[patron][i] * 1
            else:
                self.funcion_de_activacion = self.funciones_activacion_capas[c]
                for i in range(self.neuronas_capas[c]):
                    for j in range(self.neuronas_capas[c - 1]):
                        self.wc[c][j][i] += 2 * self.wc[c][j][i] * self.enl[c][i] * self.h[c - 1][j]
                    self.uc[c][i] += 2 * self.trainParam['lr'] * self.enl[c][i] * 1
        # self.mostrar_pesos_y_umbrales_actualizados()

    def mostrar_pesos_y_umbrales_actualizados(self):
        pass

    def calcular_error_rms(self):
        self.rms = sum(self.ep) / self.cantidad_patrones
        self.errores.append(self.rms)
        print('RMS: %s' % self.rms)

    def graficar_errores(self):
        ylim(-1,1)
        plot(self.errores)
        show()

    def guardar_pesos_y_umbrales(self):
        pickle.dump(self.wc, open('pesos.db', 'wb'))
        pickle.dump(self.uc, open('umbrales.db', 'wb'))

    def entrenar(self, patrones, salida_deseada):
        self.x = patrones # x[[patron 1], [patron 2], ...]
        self.yd = salida_deseada # y[[salida 1], [salida 2], ...]
        self.definir_cantidad_patrones()
        self.definir_parametros_auxiliares()
        for iteracion in range(self.trainParam['epochs']):
            for patron in range(self.cantidad_patrones):
                self.calcular_salida_de_la_red(patron)
                self.calcular_error_lineal(patron)
                self.calcular_errores_no_lineales(patron)
                self.calcular_error_del_patron(patron)
                self.actualizar_pesos_y_umbrales(patron)
            self.calcular_error_rms()
            if self.rms <= self.trainParam['goal']:
                self.graficar_errores()
                self.guardar_pesos_y_umbrales()
                break
            elif iteracion >= self.trainParam['epochs'] - 1:
                self.graficar_errores()

    def simular(self, entradas):
        pass

entradas = [
    [2, 2, 2, 2, 10, 10, 2, 2, 2, 2],
    [10,6, 6, 6,  6,  6, 6, 6, 6, 6],
    [10,4, 4, 4,  4,  4, 4, 4, 4, 4],
    [2, 2, 2, 5, 10, 10, 7, 8, 2, 2],
    [10, 10, 6, 4, 4,6,  6, 6, 6, 6]
]
salidas = [
    [1, 0, 0, 0, 0]
]

# neuronas_por_capa = []
# funciones_de_activacion = []

# cantidad_de_capas = int(input('Ingrese la cantidad de capas: '))

# for i in range(cantidad_de_capas):
#     neuronas = int(input('Ingrse la cantidad de neuronas de la capa %s: ' % (i + 1)))
#     neuronas_por_capa.append(neuronas)
#     funcion_activacion = input('Ingrse la funcion de activacion de la capa %s: ' % (i + 1))
#     funciones_de_activacion.append(funcion_activacion)

# funcion_activacion_capa_salida = input('Ingrese la funcion de activiacion de la capa de salida: ')

# red = BackpropagationMulticapa(
#     len(entradas[0]), len(salidas), 
#     neuronas_por_capa, 
#     funciones_de_activacion,
#     funcion_activacion_capa_salida
# )

# red.trainParam['epochs'] = int(input("Ingrese el numero de iteraciones: "))
# red.trainParam['lr'] = float(input('Ingrese la rata de aprendizaje: '))
# red.trainParam['goal'] = float(input('Ingrese el error maximo permitido: '))

red = BackpropagationMulticapa(len(entradas[0]), len(salidas), [3, 2], ['sigmoid', 'sigmoid'], 'sigmoid')
red.trainParam['epochs'] = 1000

red.entrenar(entradas, salidas)
# salida_red = red.simular(entradas)
